package logic;

public enum CommunicationCode {
	
	FILE_REQUEST, FILE_PERMIT_GRANTED, FILE_PERMIT_DENIED, MESSAGE, FILE, DISCONNECTED;

}
