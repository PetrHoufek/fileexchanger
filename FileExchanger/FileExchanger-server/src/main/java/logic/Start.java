package logic;

import gui.GUI;

public class Start {

	public static void main(String[] args) {

		Logic logic = new Logic();
		GUI gui = new GUI(logic);
		
		logic.setGui(gui);

	}

}
