I wrote this application in february 2016, the original purpose was to have a skype like application where people would be able to communicate in group chats. 
Mid-way I decided to change the application into something else which affected the structure and design of my code. I turned it into an application for two users only(in one session) but added the possibility of sharing very large files with each other (for example 5GB).

The client side app contacts the server side which will create a session and waits for the second user, then it continues exchanging data between them.
(You need to set your modem in order to be able to run the server for other users on your computer)

![Scheme](https://bitbucket.org/PetrHoufek/fileexchanger/downloads/a1.jpg)
![Scheme](https://bitbucket.org/PetrHoufek/fileexchanger/downloads/a2.jpg)
![Scheme](https://bitbucket.org/PetrHoufek/fileexchanger/downloads/a3.jpg)
![Scheme](https://bitbucket.org/PetrHoufek/fileexchanger/downloads/a4.jpg)
![Scheme](https://bitbucket.org/PetrHoufek/fileexchanger/downloads/a5.jpg)










![Scheme](https://bitbucket.org/PetrHoufek/fileexchanger/downloads/a6.jpg)
![Scheme](https://bitbucket.org/PetrHoufek/fileexchanger/downloads/a7.jpg)
![Scheme](https://bitbucket.org/PetrHoufek/fileexchanger/downloads/a8.jpg)
![Scheme](https://bitbucket.org/PetrHoufek/fileexchanger/downloads/a9.jpg)