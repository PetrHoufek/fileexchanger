package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import logic.Logic;

public class GUI {

	private Logic logic;

	private JButton buttonStart = new JButton("Start");

	private JButton buttonStop = new JButton("Stop");

	private JButton buttonExit = new JButton("Exit");

	private JTextField textFieldPort = new JTextField();

	private JTextArea serverTextInfo = new JTextArea();

	private JScrollPane scrollPane = new JScrollPane(serverTextInfo);

	public GUI(Logic logic) {

		this.logic = logic;
		initializeServer();

	}

	private void initializeServer() {

		JFrame frame = new JFrame();
		JPanel framePanel = new JPanel(new BorderLayout());
		JPanel outerButtonPanel = new JPanel(new GridBagLayout());
		JPanel componentPanel = new JPanel();
		JPanel textFieldPanel = new JPanel(new BorderLayout());
		ButtonListener buttonListener = new ButtonListener();

		frame.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				logic.stopServer();
			}

		});
		frame.add(framePanel);
		framePanel.add(outerButtonPanel, BorderLayout.NORTH);
		framePanel.add(textFieldPanel, BorderLayout.CENTER);
		outerButtonPanel.add(componentPanel);
		componentPanel.add(buttonStart);
		componentPanel.add(textFieldPort);
		componentPanel.add(buttonStop);
		componentPanel.add(buttonExit);
		textFieldPanel.add(scrollPane);
		buttonStart.addActionListener(buttonListener);
		buttonStop.addActionListener(buttonListener);
		buttonExit.addActionListener(buttonListener);

		textFieldPort.setPreferredSize(new Dimension(60, (int) buttonStart.getPreferredSize().getHeight()));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationByPlatform(true);
		frame.setSize(new Dimension(600, 300));
		frame.setMinimumSize(new Dimension(600, 300));
		frame.setTitle("File exchanger (server - made by Petr Houfek)");
		frame.setVisible(true);

	}

	public void appendServerTextInfo(String s) {
		serverTextInfo.append(s);
	}
	
	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == buttonStart) {
				
				if(validatePort(textFieldPort.getText())){
					logic.startServer(Integer.parseInt(textFieldPort.getText()));
				}else{
					appendServerTextInfo("Invalid port\n");
				}
				
			} else if (e.getSource() == buttonStop) {
				logic.stopServer();
			} else if (e.getSource() == buttonExit) {
				System.exit(0);
			}

		}
		
		private boolean validatePort(String port) {

			try {

				int number = Integer.parseInt(port);

				if (number > 1024 && number < 65536) {
					return true;
				} else {
					return false;
				}

			} catch (NumberFormatException e) {
				return false;
			}

		}

	}

}
