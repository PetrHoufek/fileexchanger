package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import logic.CommunicationCode;
import logic.Logic;
import logic.TextStyle;

public class GUI {

	private Logic logic;

	private JButton buttonConnect = new JButton("Connect");

	private JButton buttonDisconnect = new JButton("Disconnect");

	private JButton buttonExit = new JButton("Exit");

	private JButton buttonSendMessage = new JButton("Send");

	private JButton buttonSendFile = new JButton("Send");

	private JButton buttonChooseFile = new JButton("Open File...");

	private JTextField textFieldUserName = new JTextField();

	private JTextField textFieldIP = new JTextField();

	private JTextField textFieldPort = new JTextField();

	private JTextField textFieldMessage = new JTextField();

	private JTextField textFieldFile = new JTextField();

	private JTextPane textPane = new JTextPane();

	private StyledDocument document = textPane.getStyledDocument();

	private Style styleDefault = textPane.addStyle("defaultStyle", null);

	private Style styleWarning = textPane.addStyle("warningStyle", null);

	private Style styleCurrentUser = textPane.addStyle("currentUserTextStyle", null);

	private Style styleOtherUser = textPane.addStyle("otherUsersTextStyleStyle", null);

	private JScrollPane scrollPane = new JScrollPane(textPane);

	private JFileChooser fileChooser = new JFileChooser();

	public GUI(Logic logic) {

		this.logic = logic;
		initializeClient();

	}

	private void initializeClient() {

		JFrame frame = new JFrame();
		JPanel framePanel = new JPanel(new BorderLayout());
		JPanel buttonPanel = new JPanel(new BorderLayout());
		JPanel outerTextAreaPanel = new JPanel(new BorderLayout());

		JPanel innerTextAreaPanel = new JPanel(new BorderLayout());
		JPanel messagePanel = new JPanel(new BorderLayout(3, 0));
		JPanel filePanel = new JPanel(new BorderLayout(3, 0));
		JPanel innerFilePanel = new JPanel(new GridLayout(1, 2));
		JPanel innerButtonPanel1 = new JPanel(new BorderLayout());
		JPanel innerButtonPanel1A = new JPanel(new GridLayout(2, 1, 0, 3));
		JPanel innerButtonPanel1B = new JPanel(new GridLayout(2, 1, 0, 3));
		JPanel innerButtonPanel1B2 = new JPanel(new BorderLayout(3, 0));
		JPanel innerButtonPanel2 = new JPanel();
		JPanel innerButtonPanel2A = new JPanel(new GridLayout(2, 1, 0, 3));
		JPanel innerButtonPanel2B = new JPanel(new GridLayout(2, 1, 0, 3));
		JTabbedPane tabbedPane = new JTabbedPane();
		ButtonListener buttonListener = new ButtonListener();

		frame.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				logic.disconnect();
			}

		});
		frame.add(framePanel);
		framePanel.add(buttonPanel, BorderLayout.NORTH);
		framePanel.add(outerTextAreaPanel, BorderLayout.CENTER);
		buttonPanel.add(innerButtonPanel1, BorderLayout.WEST);
		buttonPanel.add(innerButtonPanel2, BorderLayout.EAST);
		innerButtonPanel1.add(innerButtonPanel1A, BorderLayout.WEST);
		innerButtonPanel1.add(innerButtonPanel1B, BorderLayout.CENTER);
		innerButtonPanel1A.add(new JLabel("Username:"));
		innerButtonPanel1A.add(new JLabel("IP/Port:"));
		innerButtonPanel1B.add(textFieldUserName);
		innerButtonPanel1B.add(innerButtonPanel1B2);
		innerButtonPanel1B2.add(textFieldIP, BorderLayout.CENTER);
		innerButtonPanel1B2.add(textFieldPort, BorderLayout.EAST);
		innerButtonPanel2A.add(new JLabel());
		innerButtonPanel2A.add(buttonConnect);
		innerButtonPanel2B.add(buttonExit);
		innerButtonPanel2B.add(buttonDisconnect);
		innerButtonPanel2.add(innerButtonPanel2A);
		innerButtonPanel2.add(innerButtonPanel2B);
		outerTextAreaPanel.add(innerTextAreaPanel);
		innerTextAreaPanel.add(scrollPane);
		innerTextAreaPanel.add(tabbedPane, BorderLayout.SOUTH);
		tabbedPane.add(messagePanel, "text");
		tabbedPane.add(filePanel, "file");
		messagePanel.add(textFieldMessage);
		messagePanel.add(buttonSendMessage, BorderLayout.EAST);
		filePanel.add(textFieldFile);
		filePanel.add(innerFilePanel, BorderLayout.EAST);
		innerFilePanel.add(buttonChooseFile);
		innerFilePanel.add(buttonSendFile);

		buttonConnect.addActionListener(buttonListener);
		buttonDisconnect.addActionListener(buttonListener);
		buttonExit.addActionListener(buttonListener);
		buttonChooseFile.addActionListener(buttonListener);
		buttonSendFile.addActionListener(buttonListener);
		buttonSendMessage.addActionListener(buttonListener);
		textFieldMessage.addKeyListener(new KeyAdapter() {

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {

					if (logic.isConnected()) {
						sendMessage();
					}

				}
			}

		});

		innerButtonPanel1A.setPreferredSize(new Dimension(70, 0));
		innerButtonPanel1.setPreferredSize(new Dimension(250, 0));
		innerButtonPanel2.setPreferredSize(new Dimension(200, 0));
		buttonPanel.setPreferredSize(new Dimension(0, 70));
		textFieldPort.setPreferredSize(new Dimension(50, 0));
		tabbedPane.setPreferredSize(new Dimension(0, 54));

		textPane.setBorder(new BevelBorder(BevelBorder.LOWERED));
		buttonPanel.setBorder(new EmptyBorder(5, 5, 5, 5));

		StyleConstants.setBold(styleCurrentUser, true);
		StyleConstants.setBold(styleOtherUser, true);
		StyleConstants.setForeground(styleCurrentUser, new Color(128, 0, 128));
		StyleConstants.setForeground(styleOtherUser, new Color(0, 0, 255));
		StyleConstants.setForeground(styleDefault, Color.BLACK);
		StyleConstants.setForeground(styleWarning, Color.RED);

		textPane.setEditable(false);
		textFieldFile.setEditable(false);
		textFieldFile.setDisabledTextColor(Color.GRAY);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationByPlatform(true);
		frame.setSize(new Dimension(600, 300));
		frame.setMinimumSize(new Dimension(600, 300));
		frame.setTitle("File exchanger-SNAPSHOT-1.0.1 (made by Petr Houfek)");
		frame.setVisible(true);

	}
	
	private void connect() {

		if (textFieldUserName.getText().isEmpty()) {

			try {
				document.insertString(document.getLength(), "You need to enter your username!\n", styleWarning);
			} catch (BadLocationException e) {
				e.printStackTrace();
			}

			return;

		}

		if (logic.validateIP(textFieldIP.getText())) {

			if (logic.validatePort(textFieldPort.getText())) {
				logic.connect(textFieldUserName.getText(), textFieldIP.getText(), Integer.parseInt(textFieldPort.getText()));
			} else {

				try {

					document.insertString(document.getLength(), "Provided port isn't valid!\n", styleWarning);
					textPane.setCaretPosition(document.getLength());

				} catch (BadLocationException e1) {
					e1.printStackTrace();
				}

			}

		} else {

			try {

				document.insertString(document.getLength(), "Provided IP isn't valid!\n", styleWarning);
				textPane.setCaretPosition(document.getLength());

			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}

		}

	}
	
	private void openFileChooser() {

		int status = fileChooser.showOpenDialog(null);

		if (status == 0) {

			textFieldFile.setForeground(Color.GRAY);
			textFieldFile.setText(fileChooser.getSelectedFile().getAbsolutePath());

		}

	}

	private void sendFileRequest() {

		if (!textFieldFile.getText().isEmpty() && Files.exists(Paths.get(textFieldFile.getText()))) {

			disableChatActions();
			logic.sendFileRequest(Paths.get(textFieldFile.getText()));

		} else {

			textFieldFile.setForeground(Color.RED);
			textFieldFile.setText("Choose file to be sent!");

		}

	}

	public boolean receiveFileRequest(String fileName, long fileSize) {

		disableChatActions();

		final String answer1 = "Accept";
		final String answer2 = "Refuse";
		DecimalFormat df = new DecimalFormat("#.##");
		JOptionPane optionPane = new JOptionPane("Incoming file " + fileName + "(" + df.format((double) fileSize / 1000000) + " MB)");
		JDialog dialog = optionPane.createDialog(optionPane, "File request");
		
		optionPane.setOptions(new Object[] { answer1, answer2 });
		dialog.setAlwaysOnTop(true);
		dialog.setVisible(true);

		if (((String) optionPane.getValue()).equals(answer1)) {

			enableChatActions();
			return true;

		} else {

			enableChatActions();
			return false;

		}

	}
	
	private void sendMessage() {

		if (!textFieldMessage.getText().trim().isEmpty()) {
			logic.sendMessage(textFieldMessage.getText().trim(), CommunicationCode.MESSAGE);
			textFieldMessage.setText("");
		}

	}
	
	public void insertString(String clientName, String message, TextStyle style) {

		try {

			switch (style) {

			case CURRENT_USER:
				document.insertString(document.getLength(), message, styleCurrentUser);
				break;

			case OTHER_USER:
				document.insertString(document.getLength(), message, styleOtherUser);
				break;

			case WARNING:
				document.insertString(document.getLength(), new String(message + "\n"), styleWarning);
				break;

			case DEFAULT:
				document.insertString(document.getLength(), new String(message + "\n"), styleDefault);
				break;

			}

			textPane.setCaretPosition(document.getLength());

		} catch (BadLocationException e) {
			e.printStackTrace();
		}

	}
	
	public void clearChatContent(){
		textPane.setText("");
	}

	public void enableChatActions() {

		buttonSendMessage.setEnabled(true);
		buttonSendFile.setEnabled(true);
		buttonChooseFile.setEnabled(true);
		textFieldMessage.setEnabled(true);

	}
	
	public void disableChatActions() {

		buttonSendMessage.setEnabled(false);
		buttonSendFile.setEnabled(false);
		buttonChooseFile.setEnabled(false);
		textFieldMessage.setEnabled(false);

	}
	
	public void enableTextFields() {

		textFieldUserName.setEnabled(true);
		textFieldIP.setEnabled(true);
		textFieldPort.setEnabled(false);

	}
	
	public void disableTextFields() {

		textFieldUserName.setEnabled(false);
		textFieldIP.setEnabled(false);
		textFieldPort.setEnabled(false);

	}

	public Path getFilePath() {
		return Paths.get(textFieldFile.getText());
	}
	
	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			if (e.getSource() == buttonConnect) {
				connect();
			} else if (e.getSource() == buttonDisconnect) {

				if (logic.isConnected()) {
					logic.disconnect();
				}

			} else if (e.getSource() == buttonExit) {
				System.exit(0);
			} else if (e.getSource() == buttonChooseFile) {

				if (logic.isConnected()) {
					openFileChooser();
				}

			} else if (e.getSource() == buttonSendFile) {

				if (logic.isConnected()) {
					sendFileRequest();
				}

			} else if (e.getSource() == buttonSendMessage) {

				if (logic.isConnected()) {
					sendMessage();
				}

			}

		}

	}

}
