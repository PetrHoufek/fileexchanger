package logic;

public class ClientInfo {

	private String clientUserName;

	private String clientAddress;

	public ClientInfo(String clientUserName, String clientAddress) {

		this.clientUserName = clientUserName;
		this.clientAddress = clientAddress;

	}

	public String getClientUserName() {
		return clientUserName;
	}

}
