package logic;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import gui.GUI;

public class Logic {

	private static final int BUFFER_SIZE = 1024;

	private GUI gui;

	private int port;

	private boolean serverRunning;

	public void startServer(int port) {

		if (!serverRunning) {

			this.port = port;
			serverRunning = true;
			gui.appendServerTextInfo("Server started on port: " + port);
			new Thread(new ConnectionListener()).start();

		}

	}

	public void stopServer() {

		if (serverRunning) {
			serverRunning = false;
		}

	}

	public void setGui(GUI gui) {
		this.gui = gui;
	}

	private class ConnectionListener implements Runnable {

		@Override
		public void run() {

			try (ServerSocket serverSocket = new ServerSocket(port)){

				while (serverRunning) {

					Socket socket1 = serverSocket.accept();
					Socket socket2 = serverSocket.accept();
					new Thread(new SessionTask(socket1, socket2)).start();
					gui.appendServerTextInfo("user: " + socket1.getInetAddress().getHostName() + "ip: " + socket1.getInetAddress().getHostAddress() + "\n");
					gui.appendServerTextInfo("user: " + socket2.getInetAddress().getHostName() + "ip: " + socket2.getInetAddress().getHostAddress() + "\n");
					gui.appendServerTextInfo("Session created!\n\n");

				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	private class SessionTask implements Runnable {

		private boolean connectionActive = true;

		private Socket socket1;

		private Socket socket2;

		private SessionTask(Socket socket1, Socket socket2) {

			this.socket1 = socket1;
			this.socket2 = socket2;

		}

		@Override
		public void run() {

			new Thread(new ClientInputListener(socket1, socket2)).start();
			new Thread(new ClientInputListener(socket2, socket1)).start();

		}

		private class ClientInputListener implements Runnable {

			private Socket userCurrent;

			private Socket userOther;

			public ClientInputListener(Socket userCurrent, Socket userOther) {

				this.userCurrent = userCurrent;
				this.userOther = userOther;
			}

			@Override
			public void run() {

				try {

					DataInputStream codeInput = new DataInputStream(userCurrent.getInputStream());
					DataOutputStream codeOutput = new DataOutputStream(userOther.getOutputStream());
					BufferedInputStream fileInput = new BufferedInputStream(userCurrent.getInputStream());
					BufferedOutputStream fileOutput = new BufferedOutputStream(userOther.getOutputStream());
					
					while (connectionActive && serverRunning) {

						String code = codeInput.readUTF();

						switch (code) {

						case "MESSAGE":

							String message = codeInput.readUTF();
							codeOutput.writeUTF(code);
							codeOutput.writeUTF(message);

							break;

						case "FILE_REQUEST":

							String fileNamePreview = codeInput.readUTF();
							long fileSizePreview = codeInput.readLong();

							codeOutput.writeUTF(code);
							codeOutput.writeUTF(fileNamePreview);
							codeOutput.writeLong(fileSizePreview);

							break;

						case "FILE_PERMIT_GRANTED":

							codeOutput.writeUTF(code);

							break;

						case "FILE_PERMIT_DENIED":

							codeOutput.writeUTF(code);

							break;

						case "FILE":

							byte[] buffer = new byte[BUFFER_SIZE];
							int bytesRead = 0;

							String fileName = codeInput.readUTF();
							long fileSize = codeInput.readLong();
							long remaining = fileSize;

							codeOutput.writeUTF(CommunicationCode.FILE.toString());
							codeOutput.writeUTF(fileName);
							codeOutput.writeLong(fileSize);

							while (remaining > 0 && (bytesRead = fileInput.read(buffer)) > 0) {

								fileOutput.write(buffer, 0, bytesRead);
								fileOutput.flush();
								remaining -= bytesRead;

							}

							break;

						case "DISCONNECTED":

							String messageDisconnect = codeInput.readUTF();
							codeOutput.writeUTF(code);
							codeOutput.writeUTF(messageDisconnect);
							connectionActive = false;

							break;

						}

					}

				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}

	}

}
