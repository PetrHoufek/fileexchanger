package logic;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.file.Path;
import java.util.regex.Pattern;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import gui.GUI;

public class Logic {

	private static final int BUFFER_SIZE = 1024;

	private GUI gui;

	private Socket socket;

	private boolean connected;

	private ClientInfo user;

	public void connect(String userName, String address, int port) {

		if (!connected) {

			new Thread(new ServerConnector(userName, address, port)).start();
			gui.clearChatContent();

		}
	}

	public void disconnect() {

		if (!connected) {
			return;
		}

		sendMessage(user.getClientUserName() + " disconnected", CommunicationCode.DISCONNECTED);
		connected = false;
		gui.enableTextFields();
		gui.clearChatContent();

	}

	public void sendFileRequest(Path filePath) {

		try {

			DataOutputStream output = new DataOutputStream(socket.getOutputStream());

			output.writeUTF(CommunicationCode.FILE_REQUEST.toString());
			output.writeUTF(gui.getFilePath().toFile().getName());
			output.writeLong(gui.getFilePath().toFile().length());
			gui.insertString(null, "Request was sent", TextStyle.DEFAULT);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	private void receiveFileRequest(String fileName, long fileSize) {

		playNotification();

		if (gui.receiveFileRequest(fileName, fileSize)) {
			sendMessage(null, CommunicationCode.FILE_PERMIT_GRANTED);
		} else {
			sendMessage(null, CommunicationCode.FILE_PERMIT_DENIED);
		}

	}

	private void sendFile() {

		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead = 0;

		BufferedInputStream fileInput = null;

		try {

			DataOutputStream output = new DataOutputStream(socket.getOutputStream());
			fileInput = new BufferedInputStream(new FileInputStream(gui.getFilePath().toFile()));
			BufferedOutputStream fileOutput = new BufferedOutputStream(socket.getOutputStream());

			output.writeUTF(CommunicationCode.FILE.toString());
			output.writeUTF(gui.getFilePath().toFile().getName());
			output.writeLong(gui.getFilePath().toFile().length());

			long remaining = gui.getFilePath().toFile().length();

			while (remaining > 0 && (bytesRead = fileInput.read(buffer)) > 0) {

				fileOutput.write(buffer, 0, bytesRead);
				fileOutput.flush();
				remaining -= bytesRead;

			}

		} catch (IOException e) {

			gui.insertString(null, "ERROR: File transfer failed!", TextStyle.DEFAULT);
			e.printStackTrace();

		} finally {

			try {
				fileInput.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			gui.enableChatActions();

		}

		gui.insertString(null, "File successfully sent", TextStyle.DEFAULT);

	}

	private void receiveFile() {

		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead = 0;
		File defaultFolder = new File(System.getProperty("user.home") + File.separator + "FileExchanger");

		if (!defaultFolder.exists()) {
			defaultFolder.mkdir();
		}

		BufferedOutputStream fileOutput = null;

		try {

			DataInputStream input = new DataInputStream(socket.getInputStream());
			BufferedInputStream fileInput = new BufferedInputStream(socket.getInputStream());
			fileOutput = new BufferedOutputStream(new FileOutputStream(new File(defaultFolder.getPath() + File.separator + input.readUTF())));
			long remaining = input.readLong();

			gui.insertString(null, "File transfer started...", TextStyle.DEFAULT);

			while (remaining > 0 && (bytesRead = fileInput.read(buffer)) > 0) {

				fileOutput.write(buffer, 0, bytesRead);
				fileOutput.flush();
				remaining -= bytesRead;

			}

			gui.insertString(null, "File transfer completed", TextStyle.DEFAULT);

		} catch (IOException e) {

			gui.insertString(null, "ERROR: File transfer failed!", TextStyle.DEFAULT);
			e.printStackTrace();

		} finally {

			try {
				fileOutput.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	public void sendMessage(String message, CommunicationCode code) {

		if (!connected) {

			gui.insertString(null, "You have to connect to a server!", TextStyle.DEFAULT);

			return;

		}

		try {

			DataOutputStream output = new DataOutputStream(socket.getOutputStream());

			switch (code) {

			case MESSAGE:

				message = message.replace("#", "");
				output.writeUTF(CommunicationCode.MESSAGE.toString());
				output.writeUTF(message = new String(user.getClientUserName() + ": #" + message));
				showMessage(message, TextStyle.CURRENT_USER);

				break;

			case DISCONNECTED:

				output.writeUTF(CommunicationCode.DISCONNECTED.toString());
				output.writeUTF(message);
				showMessage(message, TextStyle.CURRENT_USER);

				break;

			case FILE_PERMIT_GRANTED:

				output.writeUTF(CommunicationCode.FILE_PERMIT_GRANTED.toString());
				break;

			case FILE_PERMIT_DENIED:
				output.writeUTF(CommunicationCode.FILE_PERMIT_DENIED.toString());
				break;

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void showMessage(String message, TextStyle style) {

		if (style == TextStyle.CURRENT_USER || style == TextStyle.OTHER_USER) {

			String[] tokens = message.split("#");

			if (tokens.length == 2) {

				gui.insertString(null, tokens[0], style);
				gui.insertString(null, tokens[1], TextStyle.DEFAULT);

			} else {
				gui.insertString(null, "Processing of incoming message failed, please don't use \"#\" symbol", TextStyle.WARNING);
			}

		} else {
			gui.insertString(null, message, style);
		}

	}

	public boolean validateIP(String ip) {
		return Pattern.matches("(([01]?\\d\\d?|2[0-4]\\d|25[0-5])(\\.)){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])", ip);
	}

	public boolean validatePort(String port) {

		try {

			int number = Integer.parseInt(port);

			if (number > 1024 && number < 65536) {
				return true;
			} else {
				return false;
			}

		} catch (NumberFormatException e) {
			return false;
		}

	}

	private void playNotification() {

		try {

			Clip clip = AudioSystem.getClip();
			AudioInputStream inputStream = AudioSystem.getAudioInputStream(getClass().getResource("/sounds/notification.wav"));
			clip.open(inputStream);

			synchronized (clip) {

				clip.start();

				try {
					clip.wait(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				clip.stop();

			}

		} catch (LineUnavailableException e1) {
			e1.printStackTrace();
		} catch (UnsupportedAudioFileException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	public boolean isConnected() {
		return connected;
	}

	public void setGui(GUI gui) {
		this.gui = gui;
	}

	private class ServerConnector implements Runnable {

		private String userName;

		private String address;

		private int port;

		public ServerConnector(String userName, String address, int port) {

			this.userName = userName;
			this.address = address;
			this.port = port;

		}

		@Override
		public void run() {

			try {

				socket = new Socket();
				socket.connect(new InetSocketAddress(address, port), 5000);
				new Thread(new ServerListener()).start();
				connected = true;
				user = new ClientInfo(userName, InetAddress.getLocalHost().getHostAddress().toString());
				gui.disableTextFields();

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	private class ServerListener implements Runnable {

		@Override
		public void run() {

			try {

				DataInputStream input = new DataInputStream(socket.getInputStream());

				while (socket.isConnected()) {

					String code = input.readUTF();

					switch (code) {

					case "MESSAGE":
						showMessage(input.readUTF(), TextStyle.OTHER_USER);
						break;

					case "FILE_REQUEST":
						receiveFileRequest(input.readUTF(), input.readLong());
						break;

					case "FILE_PERMIT_GRANTED":

						gui.insertString(null, "File was accepted", TextStyle.DEFAULT);
						sendFile();

						break;

					case "FILE_PERMIT_DENIED":

						gui.enableChatActions();
						gui.insertString(null, "File was refused", TextStyle.WARNING);

						break;

					case "FILE":
						receiveFile();
						break;

					case "DISCONNECTED":

						showMessage(input.readUTF(), TextStyle.DEFAULT);
						connected = false;
						gui.enableTextFields();

						break;

					}

				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

}
